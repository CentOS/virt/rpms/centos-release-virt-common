Summary: Common release file to establish shared metadata for CentOS Virt SIG
Name: centos-release-virt-common
Epoch: 0
Version: 1
Release: 1%{?dist}
License: GPL-2.0-or-later
Group: System Environment/Base
Source0: RPM-GPG-KEY-CentOS-SIG-Virtualization
Source1: LICENSE
URL: https://sig.centos.org/virt/
BuildArch: noarch

Requires: centos-release
Requires: centos-gpg-keys

%description
Common files needed by other centos-release components in the Virtualization SIG

%prep
%setup -q -n %{name} -T -c

%build

%install
mkdir -p %{buildroot}/etc/pki/rpm-gpg/
install -m 644 %{SOURCE0} %{buildroot}/etc/pki/rpm-gpg/
# copy license here for %%license macro
mkdir -p ./docs
cp %{SOURCE1} ./docs


%files
%defattr(-,root,root)
/etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-SIG-Virtualization
%license docs/LICENSE

%changelog
* Tue Jul 02 2024 Sandro Bonazzola <sbonazzo@redhat.com> - 0:1-1
- Packaged for CentOS Stream 10
